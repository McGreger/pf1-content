_id: z0hzunCQ0cigePLW
_key: '!journal!z0hzunCQ0cigePLW'
content: >-
  <h1>Gorum</h1><h2>Our Lord in Iron</h2><b>Source</b> <a><i>Inner Sea Gods pg.
  60</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Gorum">Gorum</a><h3>Details</h3><p><b>Alignment</b>
  CN<br /><b>Pantheon</b> Core Deities<br /><b>Areas of Concern</b> Battle,
  strength, weapons<br /><b>Domains</b> Chaos, Destruction, Glory, Strength,
  War<br /><b>Subdomains</b> Blood, Duels, Ferocity, Fist, Legend*, Protean,
  Rage, Resolve, Tactics<br /><i>* Requires the <u>Acolyte of Apocrypha</u>
  trait.</i><br /><b>Favored Weapon</b> Greatsword<br /><b>Symbol</b> Sword in
  mountain<br /><b>Sacred Animal(s)</b> Rhinoceros<br /><b>Sacred Color(s)</b>
  Gray, red<br /></p><h3>Obedience</h3><p>Dress yourself in the heaviest set of
  metal armor you own. Shout your oath of loyalty to Gorum at the top of your
  lungs, punctuating each pause for breath by smashing your weapon against a
  shield or against your armor-clad body. After your oath is done, kneel on one
  knee with your weapon resting against your shoulder. Recite your victories in
  battle in a sonorous voice until the time for your obedience is done. If you
  should be attacked while conducting your obedience, slay the creature who
  dared test your might. (You may be assisted by allies, but you must strike the
  killing blow.) Gain a +4 sacred or profane bonus on Strength checks and
  Strengthbased skill checks. The type of bonus depends on your alignment-if
  you're neither good nor evil, you must choose either sacred or profane the
  first time you perform your obedience, and this choice can't be
  changed.</p><h3>Divine Gift</h3><p><b>Source</b> <a><i>Planar Adventures pg.
  76</i></a><br /><i>Nethys Note: See here for details on how to gain a Divine
  Gift</i><br />Gorum grants a permanent +1 untyped bonus to Strength.</p><h3>On
  Golarion</h3><p><b>Centers of Worship</b> Brevoy, Lastwall, Lands of the
  Linnorm Kings, Nirmathas, Numeria, Realm of the Mammoth Lords, River
  Kingdoms<br /><b>Nationality</b> Kellid</p><h2>Boons - Deific
  Obedience</h2><h3> Evangelist</h3><p><strong>Source</strong>: Inner Sea Gods
  pg. 60<br /><b>1: Weaponsmith (Sp)</b> <i>crafter's fortune</i><sup>APG</sup>
  3/day, <i>fox's cunning </i>2/day, or <i>greater magic weapon </i>1/day<br
  /><b>2: War Mount (Ex)</b> If you make a full attack while mounted, your mount
  also attacks with great enthusiasm. You must attempt a Ride check as normal to
  fight with a combat-trained mount. If your Ride check succeeds, your mount can
  attack with a +4 bonus on its attack and damage rolls.<br /><b>3: Chaotic
  Charge (Ex)</b> Three times per day, you can make a chaotic charge attack
  while mounted. You must declare your use of this ability before you roll your
  attack. You take an extra -2 penalty to AC in addition to the normal AC
  penalty for charging, but you deal an extra 2d6 points of damage to creatures
  of lawful alignment on a successful mounted charge. If you have the cavalier's
  charge, mighty charge, or supreme charge class ability, you instead deal an
  extra 3d6 points of damage to creatures of lawful alignment on a successful
  wild charge and don't take the AC
  penalty.</p><h3>Exalted</h3><p><strong>Source</strong>: Inner Sea Gods pg.
  60<br /><b>1: Battler (Sp)</b> <i>magic stone </i>3/day, <i>spiritual weapon
  </i>2/day, or <i>deadly juggernaut </i>1/day<br /><b>2: Mass Strength Surge
  (Su)</b> When using the strength surge granted power from the Strength domain,
  you can target allies within 30 feet of you instead of having to touch a
  single target. You can target a maximum number of allies equal to 1 + 1 for
  every 4 Hit Dice you possess (maximum 6). If you don't have access to the
  Strength domain, you instead gain the ability to use the strength surge
  granted power a number of times per day equal to 3 + your Wisdom modifier, as
  listed in the strength surge description. However, you can touch only a single
  target when using this granted power.<br /><b>3: Gorum's Shout (Sp)</b> Once
  per day, you can use <i>word of chaos </i>as a spell-like ability. In order to
  use this ability, you must shout a battle cry at top volume, ending your shout
  in praise to Gorum.</p><h3>Sentinel</h3><p><strong>Source</strong>: Inner Sea
  Gods pg. 60<br /><b>1: Mighty Warrior (Sp)</b> <i>enlarge person </i>3/day,
  <i>bull's strength </i>2/day, or <i>beast shape I</i> 1/day<br /><b>2:
  Two-Handed Smash (Ex)</b> If you make a full attack while wielding a
  two-handed melee weapon, you may make a single unarmed strike in addition to
  your normal attacks. In essence, after you complete your two-handed weapon
  attacks, you smash with your elbow, kick out with a foot, or make some other
  unarmed strike against an opponent. This bonus attack is made at your highest
  base attack bonus, and provokes an attack of opportunity if you lack the
  Improved Unarmed Strike feat or a similar ability. If you're Medium, you deal
  1d6 points of damage with this unarmed strike; if you're Small, you deal 1d4
  points of damage. Add half your Strength bonus to the damage dealt. The attack
  roll for the unarmed strike is subject to the normal penalties for two-weapon
  fighting unless you have the feats to reduce these penalties.<br /><b>3:
  Devout Rage (Ex)</b> You enter a holy or unholy frenzy whenever you rage,
  depending on your alignment. You gain a +2 bonus on your attack and damage
  rolls while raging. If you don't have the rage class feature, you can fly into
  a rage once per day as the <i>rage </i>spell, though you don't need to
  concentrate. Instead, the rage lasts for a number of rounds equal to your Hit
  Dice or until you choose to end it, whichever comes first.</p><h2>Divine
  Fighting Technique</h2><h3>Gorum's Swordmanship</h3><p><b>Source</b>
  <a><i>Weapon Master's Handbook pg. 11</i></a><br />Gorum teaches devastating
  greatsword techniques.</p><p><b>Optional Replacement</b>: A chaotic neutral
  barbarian or fighter who worships Gorum can replace a bonus feat or rage power
  with the following initial benefit.</p><p><b>Initial Benefit</b>: If you have
  the Vital Strike feat, you can apply its effect to an attack you make with a
  greatsword at the end of a charge. If you don't have the Vital Strike feat,
  that attack deals 1 additional point of damage instead. The first time you
  make an attack of opportunity with a greatsword after using Vital Strike with
  a greatsword on your turn, you can apply Vital Strike to that attack of
  opportunity.</p><p><b>Advanced Prerequisites</b>: Str 13, Cleave, Power
  Attack, Vital Strike, base attack bonus +10.</p><p><b>Optional
  Replacement</b>: A chaotic neutral barbarian or fighter of at least 10th level
  who worships Gorum can replace a bonus feat or rage power with the following
  advanced benefit, even if she doesn't meet the
  prerequisites.</p><p><b>Advanced Benefit</b>: When you use Vital Strike,
  Improved Vital Strike, or Greater Vital Strike with a greatsword, the damage
  counts as continuous damage from an injury for the purpose of determining
  whether a target must attempt a concentration check to cast spells (<i>Core
  Rulebook</i> 206).</p><h2>For Followers of
  Gorum</h2><h3>Archetypes</h3><p>Armor Master (Fighter), Armored Hulk
  (Barbarian), Invulnerable Rager (Barbarian)</p><h3>Feats</h3><p>Channel
  Viciousness, Charge Through, Furious Focus, Ironbound Master, To the
  Last</p><h3>Magic Items - Altars</h3><p>Altar of Gorum</p><h3>Magic Items -
  Armor</h3><p>Eternal Iron Breastplate</p><h3>Magic Items - Rings</h3><p>Ring
  of Sundering Metals</p><h3>Magic Items -
  Weapons</h3><p>Bloodlight</p><h3>Magic Items - Wondrous Items</h3><p>Iron
  Lord's Transforming Slivers, Maul of the Titans, Shad'gorum
  Nugget</p><h3>Monsters</h3><p>The First Blade (Herald),
  Zentragt</p><h3>Spells</h3><p>Gorum's Armor, Instant Armor, Lighten Object,
  Lighten Object, Mass, Swallow Your Fear</p><h3>Traits</h3><p>Battlefield
  Caster, Iron Grip, Shield-Trained, Strong Heart, Veteran of
  Battle</p><h2>Unique Spell Rules</h2><b>Source </b><a><i>Inner Sea Gods pg.
  67</i></a><br /><h3>Cleric/Warpriest</h3><p><i>Rage</i> can be prepared as a
  3rd-level spell<br /><i>Iron Body</i> can be prepared as a 8th-level spell<br
  /><i>Heat Metal</i> can be prepared as a 3rd-level spell<br /><i>Lead
  Blades</i> can be prepared as a 3rd-level spell<br
  /></p><h3>Druid</h3><p><i>Rage</i> can be prepared as a 3rd-level spell<br
  /><i>Iron Body</i> can be prepared as a 8th-level spell<br
  /></p><h3>Inquisitor</h3><p><i>Lead Blades</i> can be prepared as a 3rd-level
  spell<br /></p><h2>Unique Summon Rules</h2><b>Source </b><a><i>Pathfinder #35:
  War of the River Kings pg. 73</i></a><br /><b>Summon Monster III</b>: Iron
  Cobra - N (extraplanar, no poison)<br /><b>Summon Monster VI</b>: Bulette -
  N<br /><b>Summon Monster VII</b>: Behir - N<br /><b>Summon Monster VIII</b>:
  Gorgon - N<br /><h2>Other Rules</h2><b>Source </b><a><i>Pathfinder #35: War of
  the River Kings pg. 73</i></a><br />Followers are forbidden from casting the
  <i>rusting grasp</i> spell. Druids are permitted to wear metal armor, though
  they do not automatically gain proficiency in any other categories of armor.
  They cannot cast spells while wearing metal armor, nor does it meld with them
  when they use wild shape; druids interested in metal armor acquire a set for a
  specific beast form and have allies or slaves put it on them when it is time
  to fight.</p><p><div><div><iframe> </iframe></div>
   </div>
name: Gorum
pages: []

