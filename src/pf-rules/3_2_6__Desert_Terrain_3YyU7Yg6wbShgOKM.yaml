_id: 3YyU7Yg6wbShgOKM
_key: '!journal!3YyU7Yg6wbShgOKM'
folder: br8fwJFrbgshQXJz
name: 3.2.6. Desert Terrain
pages:
  - _id: U64kpnSp1DhSw9HZ
    _key: '!journal.pages!3YyU7Yg6wbShgOKM.U64kpnSp1DhSw9HZ'
    image: {}
    name: 3.2.6. Desert Terrain
    ownership:
      UA00c6VJjhkkAMA5: 3
      default: -1
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.XOJpjqmBzwE4R4n0]{(Index)
        Environment}</p><p>@Compendium[pf-content.pf-rules.CPUpraqn1jz9vhMh]{3.2.
        Wilderness}</p><ul><li>@Compendium[pf-content.pf-rules.QrderWyxAp6RUvtz]{3.2.1.
        Getting
        Lost}</li><li>@Compendium[pf-content.pf-rules.pcu8vDTPaIYw82SU]{3.2.2.
        Forest
        Terrain}</li><li>@Compendium[pf-content.pf-rules.zJ4qqxSB0Iufd5Be]{3.2.3.
        Marsh
        Terrain}</li><li>@Compendium[pf-content.pf-rules.RVKULXygOAXTZ1WC]{3.2.4.
        Hills
        Terrain}</li><li>@Compendium[pf-content.pf-rules.B5qjfHf1pFgEA7bg]{3.2.5.
        Mountain
        Terrain}</li><li>@Compendium[pf-content.pf-rules.tuZEOzPezZUbz4Fj]{3.2.5.2.
        Mountain
        Travel}</li><li><strong>@Compendium[pf-content.pf-rules.3YyU7Yg6wbShgOKM]{3.2.6.
        Desert
        Terrain}</strong></li><li>@Compendium[pf-content.pf-rules.QOCypcVHE6zpYlyD]{3.2.7.
        Plains
        Terrain}</li><li>@Compendium[pf-content.pf-rules.xiRC5p84P3tFW9mo]{3.2.8.
        Aquatic Terrain}</li></ul><hr /><p><strong>Source</strong>: PRPG Core
        Rulebook pg. 430</p><p>Desert terrain exists in warm, temperate, and
        cold climates, but all deserts share one common trait: little rain. The
        three categories of desert terrain are tundra (cold desert), rocky
        deserts (often temperate), and sandy deserts (often
        warm).</p><p>@Compendium[pf-content.pf-rules.meSI5igDtER7aVBn]{3.2.6.1.
        Sandstorms}</p><p>Tundra differs from the other desert categories in two
        important ways. Because snow and ice cover much of the landscape, it’s
        easy to find water. During the height of summer, the permafrost thaws to
        a depth of a foot or so, turning the landscape into a vast field of mud.
        The muddy tundra affects movement and skill use as the shallow bogs
        described in Marsh Terrain, although there’s little standing
        water.</p><p>The table below describes terrain elements found in each of
        the three desert categories. The terrain elements on this table are
        mutually exclusive; for instance, a square of tundra might contain
        either light undergrowth or an ice sheet, but not both.</p><hr
        /><table><thead><tr><td> </td><td>Desert Category</td></tr><tr><td>
        </td><td>Tundra</td><td>Rocky</td><td>Sandy</td></tr></thead><tbody><tr><td>Light
        undergrowth</td><td>15%</td><td>5%</td><td>5%</td></tr><tr><td>Ice
        sheets</td><td>25%</td><td>—</td><td>—</td></tr><tr><td>Light
        rubble</td><td>5%</td><td>30%</td><td>10%</td></tr><tr><td>Dense
        rubble</td><td>—</td><td>30%</td><td>5%</td></tr><tr><td>Sand
        dunes</td><td>—</td><td>—</td><td>50%</td></tr></tbody></table><hr
        /><h2>Light Undergrowth</h2><p>Consisting of scrubby, hardy bushes and
        cacti, light undergrowth functions as described for other terrain
        types.</p><h2>Ice Sheet</h2><p>The ground is covered with slippery ice.
        It costs 2 squares of movement to enter a square covered by an ice
        sheet, and the DC of Acrobatics checks there increases by 5. A DC 10
        Acrobatics check is required to run or charge across an ice
        sheet.</p><h2>Light Rubble</h2><p>Small rocks are strewn across the
        ground, making nimble movement more difficult. The DC of Acrobatics
        checks increases by 2.</p><h2>Dense Rubble</h2><p>This terrain feature
        consists of more and larger stones. It costs 2 squares of movement to
        enter a square with dense rubble. The DC of Acrobatics checks increases
        by 5, and the DC of Stealth checks increases by 2.</p><h2>Sand
        Dunes</h2><p>Created by the action of wind on sand, dunes function as
        hills that move. If the wind is strong and consistent, a sand dune can
        move several hundred feet in a week’s time. Sand dunes can cover
        hundreds of squares. They always have a gentle slope pointing in the
        direction of the prevailing wind and a steep slope on the leeward
        side.</p><h2>Other Desert Terrain Features</h2><p>Tundra is sometimes
        bordered by forests, and the occasional tree isn’t out of place in the
        cold wastes. Rocky deserts have towers and mesas consisting of flat
        ground surrounded on all sides by cliffs and steep slopes (as described
        in @Compendium[pf-content.pf-rules.B5qjfHf1pFgEA7bg]{3.2.5. Mountain
        Terrain}). Sandy deserts sometimes have quicksand; this functions as
        described in Marsh Terrain, although desert quicksand is a waterless
        mixture of fine sand and dust. All desert terrain is crisscrossed with
        dry streambeds (treat as trenches 5 to 15 feet wide) that fill with
        water on the rare occasions when rain falls.</p><h2>Stealth and
        Detection in the Desert</h2><p>In general, the maximum distance in
        desert terrain at which a Perception check for detecting the nearby
        presence of others can succeed is 6d6 × 20 feet; beyond this distance,
        elevation changes and heat distortion in warm deserts makes sight-based
        Perception impossible. The presence of dunes in sandy deserts limits
        spotting distance to 6d6 × 10 feet. The scarcity of undergrowth or other
        elements that offer concealment or cover makes using Stealth more
        difficult.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
    video:
      controls: true
      volume: 0.5

