_id: 35SVCkNhoYiFzlz6
_key: '!journal!35SVCkNhoYiFzlz6'
content: >-
  <h1>Thamir Gixx</h1><h2>The Silent Blade</h2><b>Source</b> <a><i>Inner Sea
  Gods pg. 324</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Thamir Gixx">Thamir
  Gixx</a><h3>Details</h3><p><b>Alignment</b> CE<br /><b>Pantheon</b> Halfling
  Deities<br /><b>Areas of Concern</b> Greed, opportunity, thievery<br
  /><b>Domains</b> Chaos, Evil, Nobility, Trickery<br /><b>Subdomains</b> Demon
  (Chaos), Demon (Evil), Leadership, Thievery, Whimsy<br /><i>* Requires the
  <u>Acolyte of Apocrypha</u> trait.</i><br /><b>Favored Weapon</b> Dagger<br
  /><b>Symbol</b> Black dagger and circle<br /><b>Sacred Animal(s)</b>
  Raccoon<br /><b>Sacred Color(s)</b> Black, brown<br
  /></p><h3>Obedience</h3><p>Place a gold coin in the middle of a busy street
  and spy on it from a hidden position. If someone picks up the coin, you must
  silently follow that person for 1 hour. At the end of that hour, judge that
  person's social standing. If the person is fortunate or wealthy, steal the
  coin back. If the person is destitute or downtrodden, let him keep the coin
  and offer a silent prayer to Thamir Gixx on his behalf. If no one picks up the
  coin after an hour, you must dispose of the coin in a way no one might ever
  find it, such as by throwing it into the sea or burying it in a hidden
  location. You gain a +2 profane bonus on Sleight of Hand and Stealth
  checks.</p><h2>Boons - Deific Obedience</h2><h3> Evangelist</h3><p><b>Source
  </b><a><i>Pathfinder #131: The Reaper's Right Hand pg. 72</i></a><br /><b>1:
  The Unseen (Sp)</b> <i>vanish</i> 3/day, <i>chameleon stride</i> 2/ day, or
  <i>invisibility sphere</i> 1/day<br /><b>2: Hidden in Darkness (Su)</b> You
  can use the Stealth skill even while being observed. As long as you are within
  an area of dim light or darker, you can hide yourself from view in the open
  without anything to actually hide behind. You can't, however, hide in your own
  shadow, and you can't use this ability to hide from creatures that have
  darkvision or have the see in darkness supernatural ability (<i>Pathfinder RPG
  Bestiary 2</i> 301).<br /><b>3: Feed on Shadows (Su)</b> Whenever you are in
  an area of dim light or darker, you can, as a standard action, regain a number
  of hit points equal to 4d8 + 1 per character level. You can use this ability a
  number of times per day equal to 3 + your Charisma modifier (minimum
  1).</p><h3>Exalted</h3><p><strong>Source</strong>: Pathfinder #131: The
  Reaper's Right Hand pg. 72<br /><b>1: The Unbowed (Sp)</b> <i>remove fear</i>
  3/day, <i>blessing of courage and life</i> 2/day, or <i>remove curse</i>
  1/day<br /><b>2: Shadow Twin (Su)</b> As a standard action, you can create a
  quasi-real duplicate of yourself using material from the Plane of Shadow. The
  double appears in a square adjacent to you. Your foes are unable to tell the
  two of you apart, but a foe that succeeds at a Will saving throw (DC = 10 +1/2
  your level + your Charisma modifier) upon interacting with your double
  identifies the double as an illusion. Your double has your Armor Class and
  saving throws and 20% of your maximum hit points. As a move action, you can
  direct your double to move at your speed and to talk and gesture as if it were
  real for 1 round; it cannot attack or cast spells, though it can pretend to do
  so. If you do not direct your double, it stands still, granting a +2
  circumstance bonus on any saving throw to disbelieve it that round. Your
  double provides flanking against foes that haven't identified it as an
  illusion, even when standing still. Your double lasts for a number of rounds
  equal to your character level. You can use this ability a number of times per
  day equal to 3 + your Charisma modifier (minimum 1), but you can only have one
  double in existence at a time.<br /><b>3: Quiet of the Grave (Su)</b> Three
  times per day, you can create a 20-foot-radius emanation of complete silence
  centered on yourself (as per the <i>silence</i> spell) that lasts for a number
  of rounds equal to your character level. When a creature is reduced to -1 or
  fewer hit points while within this radius of silence, but is still alive, you
  can attempt to kill it as an immediate action. That creature must succeed at a
  Will saving throw (DC = 10 + 1/2 your character level + your Charisma
  modifier) or it dies, you gain 2d8 temporary hit points, and your effective
  caster level increases by 1 for 10 minutes or until the next time you cast a
  spell, whichever comes first. Temporary hit points gained from this ability
  stack, but the effective caster level increases don't stack with each
  other.</p><h3>Sentinel</h3><p><b>Source </b><a><i>Pathfinder #131: The
  Reaper's Right Hand pg. 73</i></a><br /><b>1: The Unbroken (Sp)</b> <i>chill
  touch</i> 3/day, <i>cat's grace</i> 2/day, or <i>locate weakness</i> 1/day<br
  /><b>2: Dagger of Night (Su)</b> As a move action, you can conjure a blade of
  darkness into your hand. This blade of darkness functions as a dagger, but
  deals an amount of damage equal to 1d8 + 1 point per 3 character levels + your
  Strength modifier; half of this damage is piercing and the other half is cold
  damage. The blade is considered a magic weapon for the purpose of bypassing
  damage reduction. The blade lasts for a number of rounds equal to your
  character level, though the blade immediately vanishes if it leaves your hand.
  You can use this ability a number of times per day equal to 3 + your Charisma
  modifier (minimum 1); you can expend two uses of this ability with the same
  move action to conjure two daggers (wielding one in each hand) if you wish.<br
  /><b>3: Umbral Form (Su)</b> As a standard action, you can weave strands of
  shadow-stuff into your physical form. You gain DR 10/- and cold resistance 15.
  When you take damage from a critical hit or sneak attack, there is a 50%
  chance that the critical hit or sneak attack is negated and damage is instead
  rolled normally. This does not stack with similar abilities that negate
  critical hits and sneak attacks (such as <i>fortification</i> armor). You gain
  the see in darkness supernatural ability (Bestiary 2 301) allowing you to see
  in areas of even magical darkness. In areas of dim light or darker, you can
  move as if affected by <i>air walk</i>. You can return to your normal form as
  a free action. You can use this ability a number of rounds per day equal to
  your character level, and these rounds need not be consecutive.</p><h2>For
  Followers of Thamir Gixx</h2><h3>Magic Items - Wondrous Items</h3><p>Silent
  Blade Vest</p><h3>Monsters</h3><p>Fantionette</p><h3>Spells</h3><p>Halfling
  Vengeance, Halfling Vengeance, Mass, Word of
  Beckoning</p><h3>Traits</h3><p>Always Threatening, Backstabber</p><h2>Unique
  Spell Rules</h2><b>Source </b><a><i>Pathfinder #131: The Reaper's Right Hand
  pg. 72</i></a><br /><h3>Cleric/Warpriest</h3><p><i>Invisibility</i> can be
  prepared as a 2nd-level spell<br /><i>Invisibility, Greater</i> can be
  prepared as a 4th-level spell<br /></p><h2>Unique Summon Rules</h2><b>Source
  </b><a><i>Pathfinder #131: The Reaper's Right Hand pg. 72</i></a><br
  /><b>Summon Monster IV</b>: Shadow<br /><b>Summon Monster VII</b>: Greater
  Shadow</p><p><div><div><iframe> </iframe></div>
   </div>
name: Thamir Gixx
pages: []

