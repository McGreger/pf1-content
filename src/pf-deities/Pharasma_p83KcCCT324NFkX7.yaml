_id: p83KcCCT324NFkX7
_key: '!journal!p83KcCCT324NFkX7'
content: >-
  <h1>Pharasma</h1><h2>Lady of Graves</h2><b>Source</b> <a><i>Inner Sea Gods pg.
  116</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Pharasma">Pharasma</a><h3>Details</h3><p><b>Alignment</b>
  N<br /><b>Pantheon</b> Core Deities<br /><b>Other Pantheons</b> Deities of
  Tian Xia<br /><b>Areas of Concern</b> Birth, death, fate, prophecy<br
  /><b>Domains</b> Death, Healing, Knowledge, Repose, Water<br
  /><b>Subdomains</b> Ancestors, Flowing, Ice, Memory, Psychopomp (Death),
  Psychopomp (Repose), Resurrection, Souls, Thought<br /><i>* Requires the
  <u>Acolyte of Apocrypha</u> trait.</i><br /><b>Favored Weapon</b> Dagger<br
  /><b>Symbol</b> Spiraling comet<br /><b>Sacred Animal(s)</b> Whippoorwill<br
  /><b>Sacred Color(s)</b> Blue, white<br /></p><h3>Obedience</h3><p>Collect
  small bones whenever it is convenient and respectful to do so. When it comes
  time to perform your obedience, lay out the bones in a spiral. At one end of
  the spiral lay a slip of parchment on which you have written the name of
  someone newly born. At the other end of the spiral, lay a slip of parchment on
  which you have written the name of someone newly deceased. Chant hymns from
  <i>The Bones Land in a Spiral</i> while proceeding solemnly around the spiral,
  trailing a black scarf on the ground behind you. Gain a +2 profane or sacred
  bonus on attack rolls made with daggers. The type of bonus depends on your
  alignment-if you're neither good nor evil, you must choose either sacred or
  profane the first time you perform your obedience, and this choice can't be
  changed.</p><h3>Divine Gift</h3><p><b>Source</b> <a><i>Planar Adventures pg.
  79</i></a><br /><i>Nethys Note: See here for details on how to gain a Divine
  Gift</i><br />The recipient is not affected by the next effect, damage, or
  other event that would otherwise kill him.</p><h3>On
  Golarion</h3><p><b>Centers of Worship</b> Brevoy, Nex, Osirion, The Shackles,
  Thuvia, Ustalav, Varisia<br /><b>Nationality</b> Garundi</p><h2>Boons - Deific
  Obedience</h2><h3> Evangelist</h3><p><b>Source </b><a><i>Inner Sea Gods pg.
  116</i></a><br /><b>1: Preserver (Sp)</b> <i>sanctuary </i>3/day, <i>gentle
  repose </i>2/day, or <i>speak with dead </i>1/day<br /><b>2: Decomposition
  (Su)</b> You can ensure the final rest of a creature. As a standard action,
  you can touch a corpse and cause it to dissolve into black ash. A corpse
  dissolved this way cannot be raised as an undead creature by any means short
  of a <i>miracle </i>or <i>wish</i>. The black ash left behind can, however, be
  used as the 'corpse' for spells that return the dead to true life-such as
  <i>raise dead</i>-as long as the entire collection of ash is kept together.<br
  /><b>3: The Veil Is Drawn Aside (Su)</b> You gain the Extra Revelation feat,
  choosing a revelation from either your chosen mystery or the Bones mystery. If
  you don't have the revelation class feature, you instead gain a +4 sacred or
  profane bonus on saving throws against necromancy spells and death
  effects.</p><h3>Exalted</h3><p><b>Source </b><a><i>Inner Sea Gods pg.
  116</i></a><br /><b>1: Quietude (Sp)</b> <i>forced quiet</i><sup>UM</sup>
  3/day, <i>silence </i>2/day, or <i>hold person </i>1/day<br /><b>2: Strike the
  Unrestful (Su)</b> As a free action, you can grant the <i>ghost touch
  </i>weapon special ability to a weapon that you hold. If that weapon is not
  magical, it is considered magical while under the effect of this ability. This
  ability affects only weapons held in your hand; if you drop the weapon or give
  it away, the effect ends on that weapon. You can affect a weapon in this way a
  number of rounds each day equal to 1 + 1 for every 4 Hit Dice you possess
  (maximum 6 rounds). These rounds don't need to be consecutive.<br /><b>3: Ally
  from the Tomb (Sp)</b> Once per day as a standard action, you can summon a
  pair of vanth psychopomps (<i>Pathfinder RPG Bestiary 4</i> 221) and gain
  telepathy with them to a range of 100 feet. The vanths follow your commands
  perfectly for 1 minute for every Hit Die you possess before vanishing back to
  their home in the Boneyard. The vanths don't follow commands that would cause
  them to aid or permit the existence of undead, and they could attack you if
  the command is particularly
  egregious.</p><h3>Sentinel</h3><p><strong>Source</strong>: Inner Sea Gods pg.
  116<br /><b>1: Undead Slayer (Sp)</b> <i>hide from undead</i> 3/day,
  <i>defending bone </i>2/day, or <i>halt undead </i>1/day<br /><b>2: Disrupting
  Strike (Su)</b> Three times per day, you can channel disruptive energy through
  your weapon against an undead creature. You must declare your use of this
  ability before you roll your attack. If your attack hits an undead creature,
  you deal an extra 1d6 points of damage plus 1d6 points of damage for every 4
  Hit Dice you possess (maximum 6d6). If your attack misses or your target was
  not an undead creature, the use of the ability is wasted.<br /><b>3: Tethered
  to the Material (Ex)</b> You cling ferociously to life-you will go to
  Pharasma's realm when she calls you and no sooner. Once per day, you can send
  yourself into a determined state that lasts for 1 minute. While in this
  determined state, you can fall to a number of negative hit points equal to 10
  + your Constitution score before you die. If you drop to negative hit points
  while in this determined state, you can continue to act normally, and do not
  bleed each round due to taking actions. If your determined state ends while
  you still have a number of negative hit points equal to or greater than your
  Constitution score, you die instantly. If your determined state ends while you
  still have negative hit points, but the number of negative hit points is not
  equal to or greater than your Constitution score, you fall unconscious and
  gain the dying condition as normal.</p><h2>For Followers of
  Pharasma</h2><h3>Archetypes</h3><p>Exorcist (Inquisitor), Fated Guide
  (Spiritualist), Seer (Oracle), Stoic Caregiver
  (Cleric)</p><h3>Feats</h3><p>Eerie Sense, Fateful Channel, Messenger of
  Fate</p><h3>Magic Items - Altars</h3><p>Altar of Pharasma</p><h3>Magic Items -
  Armor</h3><p>Gravewatcher Chainmail</p><h3>Magic Items - Rings</h3><p>Ghost
  Battling Ring, Ring of the Faithful Dead</p><h3>Magic Items -
  Sets</h3><p>Pharasma's Command</p><h3>Magic Items - Weapons</h3><p>Fate Blade,
  Lady's Spiral, Nightpiercer</p><h3>Magic Items - Wondrous Items</h3><p>Crystal
  Ball (Detect Thoughts), Fate's Shears, Icon of the Midwife, Lady's Mercy,
  Crystal Ball (Normal), Crystal Ball (See Invisibility), Stone of Tomb Warding,
  Crystal Ball (Telepathy), Crystal Ball (True
  Seeing)</p><h3>Monsters</h3><p>Ahmuuth, Steward of the Skein
  (Herald)</p><h3>Prestige Classes</h3><p>Mortal
  Usher</p><h3>Spells</h3><p>Defending Bone, Early Judgment, Funereal Weapon,
  Necrostasis, Preserve, Smite Abomination, Spiral Ascent, Spiral
  Descent</p><h3>Traits</h3><p>Corpse Hunter, Spirit Guide, Stabilizing Touch,
  Undead Slayer (Pharasma)</p><h2>Unique Spell Rules</h2><b>Source
  </b><a><i>Inner Sea Gods pg. 123</i></a><br /><h3>Adept</h3><p><i>Augury</i>
  can be prepared as a 2nd-level spell<br /><i>Death Knell</i> can be prepared
  as a 2nd-level spell<br /><i>Speak with Dead</i> can be prepared as a
  3rd-level spell<br /></p><h3>Bard</h3><p><i>Augury</i> can be prepared as a
  2nd-level spell<br /><i>Death Knell</i> can be prepared as a 2nd-level
  spell<br /><i>Speak with Dead</i> can be prepared as a 3rd-level spell<br
  /></p><h3>Cleric/Warpriest</h3><p><i>False Life</i> can be prepared as a
  2nd-level spell<br /><i>Clairaudience/Clairvoyance</i> can be prepared as a
  3rd-level spell<br /><i>Moment of Prescience</i> can be prepared as a
  8th-level spell<br /></p><h3>Inquisitor</h3><p><i>Augury</i> can be prepared
  as a 2nd-level spell<br /><i>False Life</i> can be prepared as a 2nd-level
  spell<br /><i>Clairaudience/Clairvoyance</i> can be prepared as a 3rd-level
  spell<br /><i>Moment of Prescience</i> can be prepared as a 6th-level spell<br
  /></p><h3>Oracle</h3><p><i>False Life</i> can be prepared as a 2nd-level
  spell<br /><i>Clairaudience/Clairvoyance</i> can be prepared as a 3rd-level
  spell<br /><i>Moment of Prescience</i> can be prepared as a 8th-level spell<br
  /></p><h3>Ranger</h3><p><i>Augury</i> can be prepared as a 2nd-level spell<br
  /><i>Death Knell</i> can be prepared as a 2nd-level spell<br /><i>Speak with
  Dead</i> can be prepared as a 3rd-level spell<br
  /></p><h3>Sorcerer</h3><p><i>Augury</i> can be prepared as a 2nd-level
  spell<br /><i>Death Knell</i> can be prepared as a 2nd-level spell<br
  /><i>Speak with Dead</i> can be prepared as a 3rd-level spell<br
  /></p><h3>Wizard</h3><p><i>Augury</i> can be prepared as a 2nd-level spell<br
  /><i>Death Knell</i> can be prepared as a 2nd-level spell<br /><i>Speak with
  Dead</i> can be prepared as a 3rd-level spell</p><h2>Unique Summon
  Rules</h2><b>Source </b><a><i>Pathfinder #44: Trial of the Beast pg.
  69</i></a><br /><b>Summon Monster III</b>: Nosoi<br /><b>Summon Monster
  VI</b>: Vanth<br /><b>Summon Monster I</b>: Whippoorwill (uses eagle
  statistics)<br /><b>Summon Nature's Ally I</b>: Whippoorwill (uses eagle
  statistics)<br /><h2>Other Rules</h2><b>Source </b><a><i>Inner Sea Gods pg.
  123</i></a><br />Since Pharasma despises undead, Pharasmin clerics with the
  Death domain replace the <i>animate dead </i>domain spell with <i>speak with
  dead</i>, replace <i>create undead </i>with <i>antilife shell</i>, and replace
  <i>create greater undead </i>with <i>symbol of death</i>. Clerics with the
  Souls subdomain (<i>Advanced Player's Guide </i>96) replace the <i>animate
  dead </i>domain spell with <i>speak with dead</i>.</p><p><div><div><iframe>
  </iframe></div>
   </div>
name: Pharasma
pages: []

