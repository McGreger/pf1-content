_id: aVUL2wrJszAUzNMK
_key: '!journal!aVUL2wrJszAUzNMK'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.21. Multipurpose Spells
pages:
  - _id: cDA6HjthuQcL3TVE
    _key: '!journal.pages!aVUL2wrJszAUzNMK.cDA6HjthuQcL3TVE'
    image: {}
    name: 7.1.5.21. Multipurpose Spells
    ownership:
      UA00c6VJjhkkAMA5: 3
      default: -1
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</li><li>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</li><li>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li><strong>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</strong></li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr /><p><strong>Source</strong>
        <em>Ultimate Magic pg. 136</em></p><p>A spell that gives the caster a
        choice of multiple options should be weaker overall than a spell that
        only does one thing. First, a spell that is good at two things is much
        better than a spell that is good at one thing, so you should reduce the
        power of the former spell so the two spells remain about equal. Second,
        because bards, oracles, and sorcerers can only learn a limited number of
        spells, a spell that can do multiple things is often a better choice for
        them because it’s almost like learning multiple spells.</p><p>Examples
        of poorly designed spells with multiple, dissimilar options
        are:</p><ul><li>A general “emotions” spell that lets the caster project
        one of several emotions, each of which has a different effect on
        targets.</li><li>A fire spell that lets the caster hurl a blast of fire,
        ignite multiple arrowheads to add fire damage, or make a protective
        shield of fire.</li><li>A spell that works like bull’s strength, but
        lets the caster choose which ability score it affects.</li><li>A spell
        that either teleports the caster or can be used to send away an
        unwilling target.</li><li>A spell that deals energy damage of a type
        chosen by the caster to an area.</li></ul><p>Rather than create a
        multipurpose spell that gives a “shopping list” of effects the caster
        can choose from, keep the spell focused on one or perhaps two similar
        options. Note that there is a difference between a spell with multiple
        similar options and one with radically different options. Good examples
        of appropriate multipurpose spells are alarm (audible and mental alarms
        are still alarms), beast shape I (Small or Medium animals, specific
        benefits from a short list), fire shield (two options with basically the
        same mechanical effect, on par for a spell of its level), the summon
        monster spells (very versatile but of limited duration, with monsters of
        a lower power level than other spells of the same level).</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
    video:
      controls: true
      volume: 0.5

