_id: BipbhzlQ1Kmjo9RG
_key: '!journal!BipbhzlQ1Kmjo9RG'
folder: I2QJK0e1tzfogtkr
name: 2.11.3.2. Crowd Attitudes
pages:
  - _id: 47b5Zt1y31dAOlqt
    _key: '!journal.pages!BipbhzlQ1Kmjo9RG.47b5Zt1y31dAOlqt'
    image: {}
    name: 2.11.3.2. Crowd Attitudes
    ownership:
      UA00c6VJjhkkAMA5: 3
      default: -1
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.OiNttXSDW598DIEs]{(Index) Combat
        Rules}</p><p>@Compendium[pf-content.pf-rules.OWmmbQYIhQmw9DVg]{2.11.
        Mastering
        Combat}</p><p>@Compendium[pf-content.pf-rules.bcZ0k2bBZhVOw0Df]{2.11.3.
        Performance
        Combat}</p><ul><li>@Compendium[pf-content.pf-rules.Oh6nM47XMjljsx1Z]{2.11.3.1.
        Types of Performance
        Combat}</li><li><strong>@Compendium[pf-content.pf-rules.BipbhzlQ1Kmjo9RG]{2.11.3.2.
        Crowd
        Attitudes}</strong></li><li>@Compendium[pf-content.pf-rules.4rFaiDXGOywk46xm]{2.11.3.3.
        Crowd's Starting
        Attitude}</li><li>@Compendium[pf-content.pf-rules.3dh0rKeTWE2IZgM8]{2.11.3.4.
        Performance Combat
        Check}</li><li>@Compendium[pf-content.pf-rules.UB7hSZVCiSaGvyAP]{2.11.3.5.
        Performance Combat Check
        DC}</li><li>@Compendium[pf-content.pf-rules.HFhU2Omn2KgQ4fyN]{2.11.3.6.
        Affecting the Crowd's Attitude}</li></ul><hr
        /><p><strong>Source</strong>: Ultimate Combat pg. 153</p><p>While
        combatants do the actual fighting in performance combat, the crowd
        remains an active participant in these bouts. The audience can bolster
        or demoralize the competitors with their enthusiasm or scorn for what
        they see on the battlefield, with serious results.</p><p>A crowd’s
        attitude is similar to a nonplayer character’s attitude when a character
        uses the Diplomacy skill. The DC of performance combat checks to improve
        crowd reaction is tied to the crowd’s starting or current attitude. Each
        time the combatants do something spectacular on the battlefield, they
        have the opportunity to parlay that success into a better crowd
        reaction, but missteps can also create contempt among the
        crowd.</p><p>During performance combat, it is important for the GM to
        keep track of the crowd’s attitude toward each side of the combat. Given
        that a crowd is filled with many people shouting, clapping, booing,
        hissing, or otherwise showing their pleasure or displeasure, the exact
        crowd reaction changes from moment to moment based on events on the
        battlefield, but groups of combatants always have a reasonable idea of
        what the crowd thinks of their performance at any given time.</p><p>The
        following are the general categories of crowd reaction and attitude.
        They are listed from lowest regard to highest.</p><h2>Hostile</h2><p>The
        crowd does not like what it is seeing. Hostile crowds demoralize
        combatants in a performance combat. In these battles, while the crowd is
        hostile toward a given side, those combatants take a –2 penalty on all
        attack rolls, combat maneuver checks, ability checks, skill checks, and
        saving throws. This is a mind-affecting effect.</p><p>If the crowd is
        hostile toward a side of the combat and a member of that side fails a
        performance combat check by 5 or more, that side automatically loses the
        performance part of the combat. This can be important for the story of
        the game, or if the PCs are participating in serialized performance
        combats.</p><h2>Unfriendly</h2><p>While still biased against a side, the
        crowd reserves its most vocal disdain for failed performance combat
        checks. Unfriendly crowds demoralize combatants in a performance combat.
        In these battles, while the crowd is unfriendly toward a given side,
        those combatants take a –1 penalty on all attack rolls, combat maneuver
        checks, ability checks, skill checks, and saving throws. This is a
        mind-affecting effect.</p><h2>Indifferent</h2><p>The crowd is waiting
        for something exciting to happen. Audience members show little emotion
        other than anticipation and a desire for daring feats of combat to
        occur.</p><h2>Friendly</h2><p>The crowd is beginning to be swayed toward
        one side in the fray. Audience members cheer when that side achieves
        some impressive feat in the combat, and their reaction grants a +1
        morale bonus on all attack rolls, combat maneuver checks, ability
        checks, skill checks, and saving throws for their chosen side. This is a
        mind-affecting effect.</p><h2>Helpful</h2><p>The crowd loves what it is
        seeing from a given side. Audience members stand up, chant, cheer, and
        scream for the combatants to push on toward ultimate success. A helpful
        crowd grants its chosen champions a +2 morale bonus on all attack rolls,
        combat maneuver checks, ability checks, skill checks, and saving throws.
        This is a mind-affecting effect.</p><p>If the crowd is helpful toward
        one side of the combat and a member of that side succeeds at a
        performance combat check, that side gains a victory point.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
    video:
      controls: true
      volume: 0.5

