_id: RRG1A0rBL44Qeskg
_key: '!journal!RRG1A0rBL44Qeskg'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.12. Spell Resistance
pages:
  - _id: oCzkYWV67ynSl2um
    _key: '!journal.pages!RRG1A0rBL44Qeskg.oCzkYWV67ynSl2um'
    image: {}
    name: 7.1.5.12. Spell Resistance
    ownership:
      UA00c6VJjhkkAMA5: 3
      default: -1
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li><strong>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</strong></li><li>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</li><li>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr><p><strong>Source</strong>
        <em>Ultimate Magic pg. 132</em></p><p>Whether or not spell resistance
        applies to a spell depends mostly on whether or not it is an
        instantaneous or ongoing magical effect. Spell resistance applies to
        instantaneous magical effects (such as fireball) and ongoing magical
        effects (such as wall of fire), but not to nonmagical effects or spells
        that create nonmagical effects, whether instantaneous or ongoing. For
        example, wall of stone conjures an instantaneous wall of stone that
        cannot be dispelled; spell resistance doesn’t help you walk through the
        spell’s wall any more than it would if you tried to walk through a
        mortared stone wall in a castle—neither wall is magical, and both walls
        remain there even if you use dispel magic or antimagic field on
        them.</p><p>The general rule is that most spells allow spell resistance.
        Only when you’re deliberately designing a spell that creates a
        nonmagical object or nonmagical effect is spell resistance likely to be
        irrelevant. You can use move earth (instantaneous duration) to create a
        hill, and spell resistance won’t help you get over or through the hill
        because the spell moves the earth and thereafter stops being magical;
        likewise, you can use move earth to create a pit, and spell resistance
        won’t help you ignore the pit because it’s a nonmagical pit, just as if
        you had created it with a shovel. Magic stone adds magical power to
        stones, but spell resistance doesn’t help protect against being hit by
        the stones any more than spell resistance helps protect against a +1
        arrow because the magic is focused on the stones, not on the creature
        with spell resistance.</p><p>It’s a common trick to design a spell that
        doesn’t allow spell resistance so the caster can use it against
        creatures who have spell resistance. In many cases, the idea behind the
        design is just silly, like a spell that creates a sphere of burning oil
        and hurls it at the intended area, where it bursts in an explosion of
        flame; clearly the intent is to create a nonmagical fireball that
        bypasses spell resistance. Golems in particular are often the intended
        targets of these spell designs, as their immunity to magic ability makes
        them completely immune to any effect that allows spell resistance. You
        should avoid letting these sorts of trick spells into your campaign, as
        they meddle with the balance of encounters (some monsters are designed
        to be harder for melee characters to fight, some are designed to be
        harder for spellcasters to fight, and some are just supposed to be
        difficult all around).</p><p>Whether or not a spell allows spell
        resistance is not an indicator of the spell’s power; for most
        encounters, spell resistance isn’t a factor.</p><p>If a spell’s saving
        throw entry is marked as “(harmless)” or “(object),” the spell
        resistance entry should say that as well.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
    video:
      controls: true
      volume: 0.5

