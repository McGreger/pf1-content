_id: ORtPnTEJdt3O9xHf
_key: '!journal!ORtPnTEJdt3O9xHf'
folder: p0mBpSImsQVg3MX0
name: 1.6.06. Approximating Familiars
pages:
  - _id: CP6p9chmaa4MfniI
    _key: '!journal.pages!ORtPnTEJdt3O9xHf.CP6p9chmaa4MfniI'
    image: {}
    name: 1.6.06. Approximating Familiars
    ownership:
      UA00c6VJjhkkAMA5: 3
      default: -1
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.Ic1tlF3EE1QNW3kE]{(Index)}</p><p>@Compendium[pf-content.pf-rules.8scSEgBEtJZj0suf]{(Index)
        Basic
        Rules}</p><ul><li><p>@Compendium[pf-content.pf-rules.KTAUEzhZCUioQdqK]{1.6.
        Familiars}</p><ul><li>@Compendium[pf-content.pf-rules.veGCPC3JzsxR5MnQ]{1.6.01.
        Familiar
        Basics}</li><li>@Compendium[pf-content.pf-rules.aja8EOSAA4RagQT3]{1.6.02.
        Familiar Ability
        Descriptions}</li><li>@Compendium[pf-content.pf-rules.ObzGkbjtwZP7ZJtr]{1.6.03.
        Familiar
        List}</li><li>@Compendium[pf-content.pf-rules.iO98MfTiAuGYTEYV]{1.6.04.
        Improved Familiar
        List}</li><li>@Compendium[pf-content.pf-rules.1M5KD9yy2yLQ9RVo]{1.6.05.
        Small and Vermin
        Familiars}</li><li><strong>@Compendium[pf-content.pf-rules.ORtPnTEJdt3O9xHf]{1.6.06.
        Approximating Familiars}</strong></li></ul></li></ul><hr
        /><p><strong>Source</strong>: Animal Archives</p><p>As a player, you may
        desire a special familiar—either real, unique to your campaign world, or
        wholly imagined—that hasn’t yet been presented with specific stats. With
        your Gamemaster’s approval, however, it’s easy to “re-skin” an existing
        familiar stat block to create the exact familiar you want.</p><p>The
        easiest way is to examine the Familiars and Special Abilities table and
        try to find the animal most similar to the one in your imagination, then
        use the source listed in the Statistics column to find its full stat
        block. For many animals, you’ll be able to simply use that stat block
        for your familiar, in the same way that a parrot uses the same stats as
        a raven, and change only the flavor and descriptions. This way, you
        don’t have to worry about tweaking the creature’s mechanics, and your GM
        can rest easy knowing that the familiar is still
        balanced.</p><p>Sometimes, however, you may want an animal radically
        different from any on this list. In these cases, your best bet is to
        look at existing creatures until you find something similar to what
        you’re looking for—preferably of a low Challenge Rating. From there, you
        can change the skills and feats, add or subtract attacks, and otherwise
        sculpt the creature until it matches the picture in your imagination.
        Bear in mind that monster design is a complicated process. You’ll need
        to be familiar with the rules if you want your creature to be balanced,
        and even if you do the math perfectly, your GM may still decide the
        creature is too powerful. After all, familiars are intended to be
        relatively weak, and even a baby mastodon is going to be hard to reduce
        to an appropriate CR without designing a totally new monster, at which
        point you might as well just grab the pig familiar stats and add fur.
        The young template is also extremely useful in reducing creatures’ CRs.
        Once you’ve got the stats worked out, use the examples above to decide
        what ability your familiar grants its new master. And don’t forget to
        get your GM’s approval!</p><p>For example, let’s say you want to create
        a cardinal, because you think he’d go well with your fire wizard who
        only wears red. Since they’re both small birds, it’s easy enough to take
        the raven stat block above and simply remove the ability to talk. If you
        want a lobster, you can use the king crab, perhaps reversing its land
        and swim speeds to represent its quick movement underwater. A rat makes
        a fine guinea pig, and a weasel makes a great stoat or ermine. If you
        want to tinker further, remember that the further you range from an
        existing stat block—especially if it’s to add extra attacks or otherwise
        improve a creature—the more likely your GM is to reject it as being
        unbalanced. The rules try to balance familiars so that none are
        obviously better than the others; if yours is clearly better, it’s
        probably too powerful, and perhaps better suited for the animal
        companion class feature.</p><h2>Approximating Familiars
        (FF)</h2><p><strong>Source</strong>: Familiar Folio</p><p>While there
        are already many kinds of familiars, sometimes the available options
        don’t quite fit your vision of your character’s magical companion. With
        your Gamemaster’s approval, though, the statistics of an existing
        familiar can be repurposed to approximate the perfect familiar for your
        character.</p><h2>Simple Method</h2><p>The easiest way to formulate a
        familiar when there aren’t already stats for the creature you’re looking
        for is to examine the list and find the animal closest to the one you
        have in mind. You can then simply use the statistics of the existing
        animal as those for your desired familiar. This ensures that the
        familiar is balanced, and it doesn’t require you or the GM to tinker
        with the animal’s statistics.</p><p>When approximating familiars, the
        most important thing to realize is that a given familiar’s statistics
        can be used to represent a wide variety of other creatures of the same
        creature type, not just that species. For example, the rat is a small,
        agile mammal, so it’s a reasonable leap to use its statistics to
        represent a mouse, shrew, or other similar small mammal. Conversely,
        using the stats for a bat to represent a flying fish, despite some
        similarities, is probably suboptimal. Consider some of the Advanced
        Method options for making more significant adjustments to a
        creature.</p><p>The Approximate Familiars chart provides a list of the
        animal familiars most suitable for approximating other types of
        creatures, suggestions on what those creatures might be, and the sources
        of the statistics for the base animals. The suggestions listed beside
        each entry are not exhaustive, of course; they are merely there as
        guidelines to help you devise the perfect familiar for your
        character.</p><h2>Advanced Method</h2><p>You may want a familiar that is
        radically different from any other creature on the list. In these cases,
        your best bet is to select the familiar creature that is closest to what
        you’re looking for, then work with your GM to come up with suitable
        ability substitutions or statistical alterations that bring the
        statistics in line with your imagined familiar. (Remember, always ask
        for your GM’s approval before altering creature statistics or using
        homebrew elements in the game!)</p><p>You’ll need to be well versed in
        the rules to create a balanced creature, and even if you do the math
        perfectly, your GM may still decide the creature is too
        powerful.</p><p>Generally, the less dramatic the change to your
        familiar’s base creature, the more balanced the resulting familiar will
        be. Players should always have a concrete theme in mind before adjusting
        familiars’ statistics, preferably a concept that aims toward creating a
        familiar of an existing type that has a slightly specialized skill set.
        Creatures that are unique, though still largely natural, and tie into a
        character’s backstory can also make good concepts.</p><p>Players who are
        considering altering a pre-existing familiar should consider one or more
        of the following changes to the familiar’s statistics, which are ordered
        from least to most impactful to the game
        mechanics.</p><p><strong>Skills</strong>: Reverse-calculating where
        skill ranks went is generally easier with familiars than with other
        creatures, since familiars rarely have more than 1 Hit Die, and rarely
        have more than 1 or 2 skill ranks to begin with. Reallocating skill
        ranks can be an easy way of customizing a creature’s statistics to
        better fit your image of your familiar’s personality. For instance, to
        create a primate familiar that is good at stealing small trinkets, you
        might start with the statistics of a monkey and simply reallocate its
        skill ranks from Perception to Sleight of Hand or
        Stealth.</p><p><strong>Feats</strong>: You can easily exchange a
        pre-built familiar’s starting feats with different feats that better
        match your concept, such as the familiar feats. There are also new feats
        relating to familiars, allowing a great deal of customization and
        sometimes adding unusual mystical qualities and
        abilities.</p><p><strong>Attacks</strong>: While changing the damage
        dice for a creature’s attacks can quickly create an underwhelming or
        overpowered familiar, exchanging natural attacks for other types of
        natural attacks is generally a safe practice. Using the natural attacks
        table, you can easily switch out a bat’s bite attack for two claws or a
        gore attack. Generally, you should replace a primary natural attack with
        another primary natural attack, and secondary attacks with other
        secondary attacks.</p><p><strong>Speed</strong>: Changing a creature’s
        movement types or speeds is usually to be avoided. However, it can be
        relatively safe as long as you are exchanging an unusual movement speed
        (such as a 10-foot burrow speed) for a different unusual movement speed
        of an equal rate (such as a 10-foot climb speed), or you are drastically
        reducing the creature’s base land speed to give it an unusual movement
        speed (such as reducing a creature’s 40-foot base land speed to 10 feet
        and granting the creature a 30- foot swim speed).</p><p><strong>Ability
        Scores</strong>: Altering a creature’s ability scores is the surest way
        to accidentally create an unbalanced creature, and isn’t recommended for
        approximating new familiars in this manner.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
    video:
      controls: true
      volume: 0.5

